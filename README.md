# Temperature  REST Service



This SpringBoot app a provides a simple REST API for storing temperature readings. Data is written to AWS DynamoDB.

## Testing
The application has unit and integration tests. These are run by separate Maven profiles.

### Run Unit Tests  
The following command will run the unit tests. These tests must be in classes named ```*Test.java```

``` mvn clean test -P unit-test```  
  
### Run Integration Tests  
The following command will run the integration tests. These tests must be in classes named ```*IT.java```
This profile will start a local DynamoDB before running the tests. The local DynamoDB will be stopped after the tests have completed.
When this profile is run on Gitlab CI the DynamoDB instance is not started due to this flag ```-Dskip.startlocaldynamo=true```


```mvn clean install -P integration-test```

Run stand alone application  
  
* Run in EC2 and connect to AWS DynamoDB using an IAM Role to give permission for accessing DynamoDB  

```java -jar -Dspring.profiles.active=PRD target/temperature-service-1.2-SNAPSHOT.jar```  

* Run locally and connect to local DynamoDB. Local DynamoDB could be a Docker image.

```java -jar -Dspring.profiles.active=LOCAL target/temperature-service-1.2-SNAPSHOT.jar```  

* Run locally and connect to AWS DynamoDB. Need to set the AccessKey and Secret in the configuration file.

```java -jar -Dspring.profiles.active=LOCAL-AWS target/temperature-service-1.2-SNAPSHOT.jar```  
  
  
### Run Dynamo DB Local  
  
If running locally you can use a local DynamoDB running in Docker. Start the instance as follows. Once running the Spring Boot application can connect to http://localhost:8000 to access the local DynamoDB.

```docker run -p 8000:8000 amazon/dynamodb-local```  
  

  
